import React from "react";
import { screen, render } from "@testing-library/react";
import {WebPage} from "./";

describe("WebPage",() =>{
    it("must display a title", ()=>{
        render(<WebPage/>);
        expect(screen.queryByText(/web page/i)).toBeInTheDocument();
    })
})